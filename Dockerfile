# Thanks to https://github.com/William-Yeh/docker-ansible/blob/master/ubuntu18.04/Dockerfile

FROM ubuntu:18.04

LABEL maintainer="Guillaume Hanique <guillaume@hanique.com>"

RUN echo "===> Adding Ansible's PPA..."  && \
    apt-get update  &&  apt-get install -y gnupg2    && \
    echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu bionic main" | tee /etc/apt/sources.list.d/ansible.list           && \
    echo "deb-src http://ppa.launchpad.net/ansible/ansible/ubuntu bionic main" | tee -a /etc/apt/sources.list.d/ansible.list    && \
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 7BB9C367    && \
    DEBIAN_FRONTEND=noninteractive  apt-get update  && \
    \
    \
    echo "===> Installing Ansible..."  && \
    apt-get install -y ansible=2.9.4-1ppa~bionic && \
    \
    \
    echo "===> Installing handy tools (not absolutely required)..."  && \
    apt-get install -y python-pip              && \
    pip install --upgrade pycrypto pywinrm     && \
    apt-get install -y sshpass openssh-client  && \
    \
    \
    echo "===> Removing Ansible PPA..."  && \
    rm -rf /var/lib/apt/lists/*  /etc/apt/sources.list.d/ansible.list

RUN ssh-keygen -t rsa -f /root/.ssh/id_rsa

WORKDIR /etc/ansible

# default command: display Ansible version
ENTRYPOINT [ "ansible-playbook" ]
CMD [ "playbook.yml" ]
