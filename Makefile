MAINTAINER=ghanique
IMAGENAME=ansible
VERSION=2.9.4.0

.PHONY: help
help:
	@echo "Please use any of the following targets:"
	@echo "    build        Build the dockerfile. Tag it with '${MAINTAINER}/${IMAGENAME}'."
	@echo "    run          Run the image '${MAINTAINER}/${IMAGENAME}'."
	@echo "    runbash      Run bash in the image '${MAINTAINER}/${IMAGENAME}'."

.PHONY: build
build:
	docker build -t ${MAINTAINER}/${IMAGENAME} -t ${MAINTAINER}/${IMAGENAME}:${VERSION} .

.PHONY: run
run:
	docker run --rm -ti ${MAINTAINER}/${IMAGENAME}

.PHONY: runbash
runbash:
	docker run --rm -it --entrypoint=/bin/bash ${MAINTAINER}/${IMAGENAME}
